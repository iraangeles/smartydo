// SMARTYDO project
//
databaseTask
var express = require('express');
var path = require("path");
var app = express();

var databaseTask = [];
var taskObject = {};

databaseTask = [   {"Name": "Task 1","Desc":"Task 1 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 2 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 3 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 4 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 5 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 6 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 7 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 8 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 9 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 10 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 11 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 12 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 13 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 14 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 15 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 16 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 17 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                        {"Name": "Task 1","Desc":"Task 18 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"}    
                    ];



var createTask = function(tmp){
    return ({
        name : tmp.name,
        taskdate : tmp.taskdate,
        description : tmp.description,
        priority : tmp.priority,
        category : tmp.category,
        notify : tmp.notify,
        status : tmp.status
    });
};
 


// Configure port
app.set("port",parseInt(process.argv[2]) || process.env.APP_PORT || 3000);



//Set bower route
app.use("/libs",express.static(path.join(__dirname,"bower_components")));

app.get("/getalltask", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(databaseTask);
});

app.get("/addtask", function(req, resp){
    databaseTask.push(createTask(req.query));
    console.log("All orders:\n %s", JSON.stringify(databaseTask));
    
	resp.status(201).end();
});


//Configure routes
app.use(express.static(path.join(__dirname,"public")));

app.listen(app.get("port"),function(){
    console.log("SMARTYDO Application started at %s on port %d", new Date(),app.get("port"));

}
);


