// Pending page 
( function(){

    console.log("Pending Task");
    var SmartydoPendingApp = angular.module("SmartydoPendingApp",[]);

    var SmartydoPendingCtrl = function() {

        var smartydoPendingCtrl = this;


        // Data Structure
        // var smartydoData = [{"Name": "Task 1",
        //                      "Desc":"Task 1 Description",
        //                      "Date": "10/02/17",
        //                      "status":"Pending",     : Pending, Completed, Archived 
        //                      "priority":"Low",       : Low, Med, High
        //                      "category": "Work",     : ["Work","Social","Family","Friends","None"] 
        //                      "notify":"No",          : Yes, No
        //                      "email":"email@email.com"
        //                      }];


        // smartydoCtrl.data = [{"Name": "Task 1","Desc":"Task 1 Description",
        //                         "Date": "10/02/17","status":"pending","priority":"low","notify":"no","email":"email@email.com"}];


        smartydoPendingCtrl.data = [   {"Name": "Task 1","Desc":"Task 1 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 2 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 3 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 4 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 5 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 6 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 7 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 8 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 9 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 10 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 11 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 12 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 13 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 14 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 15 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 16 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 17 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 18 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"}    
                            ];




    }

    //Define a controller call FirstCtrl -
    SmartydoPendingApp.controller("SmartydoPendingCtrl",[ SmartydoPendingCtrl ]);


})();