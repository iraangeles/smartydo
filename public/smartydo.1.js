// for client side we use IIFE to execute the application
(function() {
    console.log("Client Side SmartyDo App");
    // everything inside this function is our application
    // Create an instance of Angular application
    //1st param: app name, the second is the app dependencies
    var SmartydoApp = angular.module("SmartydoApp",[]);


    var SmartydoCtrl = function($http, $log) {
        // hold a reference of this controller so that when 'this' changes we are 
        //still referencing the controller
        var smartydoCtrl = this;

        // Data Structure
        // var smartydoData = [{"Name": "Task 1",
        //                      "Desc":"Task 1 Description",
        //                      "Date": "10/02/17",
        //                      "status":"Pending",     : Pending, Completed, Archived 
        //                      "priority":"Low",       : Low, Med, High
        //                      "category": "Work",     : ["Work","Social","Family","Friends","None"] 
        //                      "notify":"No",          : Yes, No
        //                      "email":"email@email.com"
        //                      }];


        // smartydoCtrl.data = [{"Name": "Task 1","Desc":"Task 1 Description",
        //                         "Date": "10/02/17","status":"pending","priority":"low","notify":"no","email":"email@email.com"}];

        var createTaskObject = function(tmp){
            return ({
                name : tmp.tmpTaskName,
                date : tmp.tmpTaskDate,
                description : tmp.tmpTaskDesc,
                priority : tmp.tmpTaskPrio,
                category : tmp.tmpTaskCate,
                notify : tmp.tmpTaskNoti,
                status : tmp.tmpTaskStatus
            });
        };

        var initTask = function (tmp) {
                tmp.name = "",
                tmp.date = "",
                tmp.description = "",
                tmp.priority = "",
                tmp.category = "",
                tmp.notify = false,
                tmp.status = ""
        } ;

        smartydoCtrl.tmpTaskName = "";
        smartydoCtrl.tmpTaskDate = "";
        smartydoCtrl.tmpTaskDesc = "";
        smartydoCtrl.tmpTaskPrio = "";
        smartydoCtrl.tmpTaskCate = "";
        smartydoCtrl.tmpTaskNoti = "";
        smartydoCtrl.tmpTaskStatus = "Active";

        smartydoCtrl.data = [   {"Name": "Task 1","Desc":"Task 1 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 2 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 3 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 4 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 5 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 6 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 7 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 8 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 9 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 10 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 11 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 12 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 13 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 14 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 15 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 16 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 17 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"},
                                {"Name": "Task 1","Desc":"Task 18 Description","Date": "10/02/17","status":"pending","priority":"low","notify":"no"}    
                            ];


        smartydoCtrl.taskPriority = ["High","Medium", "Low"];
        smartydoCtrl.taskCategory = ["Work","Social","Family","Friends","None"];

        //define first model of thes controller/vifew model vm
        smartydoCtrl.lowTaskName = ["Task name low 1","Task name low 2","Task name low 3","Task name low 4"];
        smartydoCtrl.lowTask = smartydoCtrl.lowTaskName.length;

        smartydoCtrl.medTaskName = ["Task name medium 1","Task name medium 2","Task name medium 3","Task name medium 4","Task name medium 5","Task name medium 6"];
        smartydoCtrl.medTask = smartydoCtrl.medTaskName.length;

        smartydoCtrl.highTaskName = ["Task name 1","Task name 2"];
        smartydoCtrl.highTask = smartydoCtrl.highTaskName.length;        

        smartydoCtrl.pendingTask =   smartydoCtrl.lowTask + smartydoCtrl.medTask + smartydoCtrl.highTask ;


        smartydoCtrl.doneTaskName = ["Task name complete 1","Task name complete 2","Task name complete 3",
                                    "Task name complete 4","Task name complete 5","Task name complete 6",
                                    "Task name complete 7","Task name complete 8","Task name complete 9"];
        smartydoCtrl.doneTask = smartydoCtrl.doneTaskName.length;


        // initTask(smartydoCrtl);

        // smartydoCtrl.getCreateCate = function(selection) {
        //     console.log(selection);

        // }

        smartydoCtrl.createTaskSave = function() {
            // var smartydoCtrl = this;

            // initTask(smartydoCrtl);

            console.log(smartydoCtrl.tmpTaskName);
            console.log(smartydoCtrl.tmpTaskDate);
            console.log(smartydoCtrl.tmpTaskDesc);
            console.log(smartydoCtrl.tmpTaskPrio);
            console.log(smartydoCtrl.tmpTaskCate);
            console.log(smartydoCtrl.tmpTaskNoti);                        

            $http.get("/addtask",{
                // params : createTaskObject(smartydoCtrl)
                params : {
                name : smartydoCtrl.tmpTaskName,
                taskdate : smartydoCtrl.tmpTaskDate,
                description : smartydoCtrl.tmpTaskDesc,
                priority : smartydoCtrl.tmpTaskPrio,
                category : smartydoCtrl.tmpTaskCate,
                notify : smartydoCtrl.tmpTaskNoti,
                status : smartydoCtrl.tmpTaskStatus}
             }).then(function(){
                //  initTask(smartydoCrtl);
                smartydoCtrl.tmpTaskName = "";
                smartydoCtrl.tmpTaskDate = "";
                smartydoCtrl.tmpTaskDesc = "";
                smartydoCtrl.tmpTaskPrio = "";
                smartydoCtrl.tmpTaskCate = "";
                smartydoCtrl.tmpTaskNoti = "";
                smartydoCtrl.tmpTaskStatus = "Active";

             });

        };

        //define another model
        // firstCtrl.reverseMyText = "";

        //define an event for the click button
        // smartydoCtrl.fillTable = function () {
            // smartydoCtrl.Text = reverse(firstCtrl.myText);
            // console.log("test " + firstCtrl.reverseMyText);
            // console.log(firstCtrl.myText);
        // }


        //define an event for the click button
        // firstCtrl.clearText = function () {
            // firstCtrl.reverseMyText =   "";
            // console.log("test " + firstCtrl.reverseMyText);
            // console.log(firstCtrl.myText);
        // }


    }

    //Define a controller call FirstCtrl -
    SmartydoApp.controller("SmartydoCtrl",["$http", "$log", SmartydoCtrl ]);

})();

